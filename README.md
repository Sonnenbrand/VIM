# VIM

Infos für VIM

## Update 2024
### Basic Setting
https://www.freecodecamp.org/news/vimrc-configuration-guide-customize-your-vim-editor/

### Plugins
```bash
mkdir -p ~/.vim ~/.vim/autoload ~/.vim/backup ~/.vim/colors ~/.vim/plugged
```

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
#### NerdTress
https://github.com/preservim/nerdtree

## Standard Settings aus der c't LINUX 2019 übernommen
set nocompatible

syntax on

filetype indent plugin on

set number

set hlsearch

set mouse=a

set ruler

set background=dark
